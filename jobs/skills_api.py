import urllib.parse
import difflib
import requests
from rest_framework import status
from rest_framework.exceptions import ValidationError


class BaseAPI:
    base_url: str = None

    def __init__(self):
        self.http = requests.Session()

    def get(self, path, raise_exception=False, **kwargs):
        rv = self.call('GET', path, raise_exception=raise_exception, **kwargs)
        return rv

    def call(self, method, path, raise_exception=False, **kwargs):
        response = self.make_request(method, path, **kwargs)
        response.data = response.json()

        if raise_exception and not status.is_success(response.status_code):
            error = response.data.get('error') or {}
            raise ValidationError(detail=error.get('message'),
                                  code=error.get('code'), )
        return response

    def make_request(self, method: str, path: str, **kwargs):
        full_url = self.get_full_url(path)
        request = self.http.request(method.lower(), full_url, **kwargs)
        return request

    def get_full_url(self, path: str):
        return urllib.parse.urljoin(self.base_url, path)


class SkillsAPI(BaseAPI):
    base_url = 'http://api.dataatwork.org/v1/'

    def find_job_by_title(self, title: str, raise_exception=False):
        jobs: list = self.find_jobs_by_title(title,
                                             raise_exception=raise_exception, )
        if not jobs:
            return None

        titles = [job.get('suggestion') for job in jobs]
        try:
            best_matches = difflib.get_close_matches(title, titles)[0]
            best_matches_index = titles.index(best_matches)
        except (ValueError, KeyError):
            best_matches_index = 0
        return jobs[best_matches_index]

    def find_jobs_by_title(self, title: str, raise_exception=False):
        params = {'begins_with': title}
        response = self.get('jobs/autocomplete',
                            params=params,
                            raise_exception=raise_exception, )
        rv = getattr(response, 'data', None) or []
        return rv

    def get_related_skills(self, job_uuid: str, raise_exception=False):
        path = 'jobs/{}/related_skills'.format(job_uuid)
        response = self.get(path, raise_exception=raise_exception, )
        job = getattr(response, 'data', None) or {}
        rv = job.get('skills', None) or []
        return rv

    def get_related_jobs(self, job_uuid: str, raise_exception=False):
        path = 'jobs/{}/related_jobs'.format(job_uuid)
        response = self.get(path, raise_exception=raise_exception, )
        job = getattr(response, 'data', None) or {}
        rv = job.get('related_job_titles', None) or []
        return rv


skills_api = SkillsAPI()
