# Generated by Django 2.1.2 on 2018-10-15 09:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='skill',
            options={'ordering': ('-score',), 'verbose_name': 'skill', 'verbose_name_plural': 'skills'},
        ),
    ]
