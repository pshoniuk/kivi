from django.db.models.signals import post_save
from django.dispatch import receiver
from .skills_api import skills_api
from .models import Job, Skill
from .utils import compute_score


def _normalize_skill_data(data):
    return {
        'uuid': data['skill_uuid'],
        'name': data['skill_name'],
        'type': data['skill_type'],
        'description': data['description'],
        'normalized_name': data['normalized_skill_name'],
        'importance': data['importance'],
        'level': data['level'],
    }


# noinspection PyUnusedLocal
@receiver(post_save, sender=Job)
def save_job(sender, instance: Job, created, **kwargs):
    if not created:
        return

    related_skills_data = skills_api.get_related_skills(instance.uuid)
    related_skills = []

    for data in related_skills_data:
        normalized_data = _normalize_skill_data(data)
        importance = normalized_data.get('importance')
        level = normalized_data.get('level')
        normalized_data['score'] = compute_score(importance, level)
        uuid = normalized_data.pop('uuid')

        skill, _ = Skill.objects.update_or_create(uuid=uuid,
                                                  defaults=normalized_data)
        related_skills.append(skill)
        skill.save()

    instance.skills.set(related_skills)
