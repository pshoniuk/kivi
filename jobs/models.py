from django.db import models


class LifeCycleDates(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class Skill(LifeCycleDates):
    uuid = models.CharField(max_length=32, unique=True, db_index=True)
    name = models.CharField(max_length=1024)
    type = models.CharField(max_length=128)
    description = models.CharField(max_length=1024)
    normalized_name = models.CharField(max_length=512)
    importance = models.FloatField()
    level = models.FloatField()
    score = models.FloatField()

    class Meta:
        ordering = ('-score',)
        verbose_name = 'skill'
        verbose_name_plural = 'skills'

    def get_score(self):
        return self.level

    def __str__(self):
        return 'skill -> {}'.format(self.pk)


class Job(LifeCycleDates):
    uuid = models.CharField(max_length=32, unique=True, db_index=True)
    title = models.CharField(max_length=1024)
    location = models.CharField(max_length=1024)
    normalized_title = models.CharField(max_length=1024)
    skills = models.ManyToManyField(to=Skill, related_name='jobs', blank=True)

    class Meta:
        ordering = ('created_at',)
        verbose_name = 'job'
        verbose_name_plural = 'jobs'

    def __str__(self):
        return 'job -> {}'.format(self.pk)
