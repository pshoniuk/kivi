from django.apps import AppConfig


class JobsConfig(AppConfig):
    name = 'jobs'

    # noinspection PyUnresolvedReferences
    def ready(self):
        import jobs.signals
