from django.contrib import admin
from .models import Job, Skill


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('pk', 'uuid', 'title',)
    search_fields = ('pk', 'uuid', 'title', 'normalized_title',)


@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = ('pk', 'uuid', 'type', 'name')
    search_fields = ('pk', 'uuid', 'type', 'name',)
