from django.urls import include, re_path
from rest_framework import routers
from .jobs.views import JobViewSet
from .skills.views import TopSkillsAPIView

app_name = 'api'
router = routers.DefaultRouter()

router.register('jobs', JobViewSet, base_name='jobs')

urlpatterns = [
    re_path(r'^', include(router.urls)),
    re_path(r'jobs/(?P<uuid>.+?)/top_skills/?$', TopSkillsAPIView.as_view(),
            name='top_job_skills'),
]
