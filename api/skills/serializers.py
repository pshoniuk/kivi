from rest_framework import serializers
from jobs.models import Skill
from jobs.utils import compute_score


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ('uuid', 'name', 'type', 'description', 'normalized_name',
                  'importance', 'level', 'score',)
        read_only_fields = ('uuid', 'score',)

    def create(self, validated_data):
        uuid = validated_data.pop('uuid')
        validated_data['score'] = self.get_score(validated_data)
        instance, _ = Skill.objects.update_or_create(uuid=uuid,
                                                     defaults=validated_data)
        return instance

    def update(self, instance, validated_data):
        importance = validated_data.get('importance')
        level = validated_data.get('level')
        if importance != instance.importance or level != instance.level:
            validated_data['score'] = self.get_score(validated_data)
        return super().update(instance, validated_data)

    # noinspection PyMethodMayBeStatic
    def get_score(self, validated_data):
        importance = validated_data.get('importance')
        level = validated_data.get('level')
        rv = compute_score(importance, level)
        return rv
