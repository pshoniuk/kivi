from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from jobs.models import Job
from .serializers import SkillSerializer


class TopSkillsAPIView(ListAPIView):
    serializer_class = SkillSerializer
    pagination_class = None

    def get_queryset(self):
        job_uuid = self.kwargs.get('uuid')
        job = Job.objects.get(uuid=job_uuid)
        return job.skills.all()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())[:10]
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
