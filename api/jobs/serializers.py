from rest_framework import serializers
from rest_framework.exceptions import NotFound
from jobs.models import Job
from jobs.skills_api import skills_api
from api.skills.serializers import SkillSerializer


class JobListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('uuid', 'title', 'location', 'normalized_title',)


class JobDetailSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(many=True, read_only=True, )

    class Meta:
        model = Job
        fields = ('uuid', 'title', 'location', 'normalized_title', 'skills',)
        read_only_fields = ('uuid',)
        extra_kwargs = {
            'normalized_title': {'required': False, },
        }

    def create(self, validated_data):
        data = self.fetch_data(validated_data.get('title'))
        validated_data['normalized_title'] = data['normalized_job_title']
        instance, _ = Job.objects.update_or_create(uuid=data['uuid'],
                                                   defaults=validated_data)
        return instance

    # noinspection PyMethodMayBeStatic
    def fetch_data(self, title: str):
        found_job = skills_api.find_job_by_title(title, raise_exception=True)
        if not found_job:
            msg = 'No job title suggestions found'
            raise NotFound(detail=msg)
        return found_job


# noinspection PyAbstractClass
class RelatedJobSerializer(serializers.Serializer):
    title = serializers.CharField()
