from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from jobs.models import Job
from jobs.skills_api import skills_api
from .serializers import JobListSerializer, JobDetailSerializer, \
    RelatedJobSerializer


class JobViewSet(ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobDetailSerializer
    lookup_field = 'uuid'

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = JobListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = JobListSerializer(queryset, many=True)
        return Response(serializer.data)

    # noinspection PyUnusedLocal
    @action(detail=True, methods=['GET'])
    def related_jobs(self, request, uuid):
        data = skills_api.get_related_jobs(uuid, raise_exception=True)
        serializer = RelatedJobSerializer(data, many=True)
        return Response(serializer.data)
