# How to run

- install dependencies from `requirements.txt`
- migrate database `python3 ./manage.py migrate`
- run server `python3 ./manage.py runserver 8000`
- enjoy